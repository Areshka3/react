import React, { useRef } from "react";
import { CSSTransition } from "react-transition-group";

const CustomCSSTransition = ({ children, ...props }) => {
  const nodeRef = useRef(null);

  return (
    <CSSTransition
      nodeRef={nodeRef}
      timeout={{
        enter: 700,
        exit: 500,
      }}
      classNames="alert"
      mountOnEnter
      unmountOnExit
      {...props}
    >
      <div ref={nodeRef}>{children}</div>
    </CSSTransition>
  );
};

export default CustomCSSTransition;
