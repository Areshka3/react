import React, { useContext } from "react";
import { AlertContext } from "../../context/alert/alertContext";
import CustomCSSTransition from "../CustomCSSTransition";

const Alert = () => {
  const { alert, hideAlert } = useContext(AlertContext);

  return (
    <CustomCSSTransition in={alert.visible}>
      <div
        className={`alert alert-${
          alert.type || "warning"
        } alert-dismissible mt-3`}
      >
        <strong>Attention!</strong> {alert.text}
        <button
          type="button"
          className="close"
          data-dismiss="alert"
          aria-label="Close"
          onClick={hideAlert}
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </CustomCSSTransition>
  );
};

export default Alert;
