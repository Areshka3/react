import React, { useContext } from "react";
import { TransitionGroup } from "react-transition-group";
import CustomCSSTransition from "../CustomCSSTransition";
import { AlertContext } from "../../context/alert/alertContext";

const Notes = ({ notes, onRemove, onToggle }) => {
  const { showAlert } = useContext(AlertContext);

  const handleRemove = (id) => {
    onRemove(id);
    showAlert(`Deleted note with ${id}`, "danger");
  };

  return (
    <TransitionGroup component="ul" className="list-group">
      {notes
        .map((note) => (
          <CustomCSSTransition key={note.id} classNames={"note"} timeout={800}>
            <li className="list-group-item note">
              <div className={note.completed && "note-done"}>
                <input
                  type="checkbox"
                  onChange={() => onToggle(note.id, note.completed)}
                  checked={note.completed && "checked"}
                />
                <strong>
                  {note.title} {`Completed: ${note.completed}`}
                </strong>
                <small>{new Date(note.date).toLocaleString()}</small>
              </div>
              <button
                type="button"
                className="btn btn-outline-danger btn-sm"
                onClick={() => handleRemove(note.id)}
              >
                &times;
              </button>
            </li>
          </CustomCSSTransition>
        ))
        .reverse()}
    </TransitionGroup>
  );
};

export default Notes;
