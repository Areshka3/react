import actionTypes from "./actionTypes";

export const firebaseReducer = (state, action) => {
  switch (action.type) {
    case actionTypes.SHOW_LOADER:
      return {
        ...state,
        loading: true,
      };

    case actionTypes.GET_NOTES:
      return {
        ...state,
        notes: action.payload,
        loading: false,
      };

    case actionTypes.ADD_NOTE:
      return {
        ...state,
        notes: [...state.notes, action.payload],
      };

    case actionTypes.REMOVE_NOTE:
      return {
        ...state,
        notes: state.notes.filter((note) => note.id !== action.payload),
      };

    case actionTypes.TOGGLE_NOTE:
      return {
        ...state,
        notes: state.notes.map((note) => {
          if (note.id === action.payload) {
            console.log(note.id, action.payload);
            note.completed = !note.completed;
          }
          return note;
        }),
      };

    default:
      return state;
  }
};
