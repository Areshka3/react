import ActionTypes from "./actionTypes";

export const showLoaderAction = () => {
  return {
    type: ActionTypes.SHOW_LOADER,
  };
};

export const fetchNotesAction = (notes) => {
  return {
    type: ActionTypes.GET_NOTES,
    payload: notes,
  };
};

export const addNoteAction = (note, id) => {
  return {
    type: ActionTypes.ADD_NOTE,
    payload: { ...note, id },
  };
};

export const removeNoteAction = (id) => {
  return {
    type: ActionTypes.REMOVE_NOTE,
    payload: id,
  };
};

export const toggleNoteAction = (id) => {
  return {
    type: ActionTypes.TOGGLE_NOTE,
    payload: id,
  };
};
