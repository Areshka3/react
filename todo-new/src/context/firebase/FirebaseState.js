import React, { useReducer } from "react";
import axios from "axios";
import { API_CONFIG } from "../../config/api.config";
import { FirebaseContext } from "./firebaseContext";
import { firebaseReducer } from "./firebaseReducer";
import {
  fetchNotesAction,
  showLoaderAction,
  addNoteAction,
  removeNoteAction,
  toggleNoteAction,
} from "./actions";

export const FirebaseState = ({ children }) => {
  const initialState = {
    notes: [],
    loading: false,
  };

  const [state, dispatch] = useReducer(firebaseReducer, initialState);

  const showLoader = () => dispatch(showLoaderAction());

  const fetchNotes = async () => {
    showLoader();

    const { data } = await axios.get(`${API_CONFIG.baseUrl}/notes.json`);

    const notes = data
      ? Object.keys(data).map((key) => {
          return { ...data[key], id: key };
        })
      : [];

    dispatch(fetchNotesAction(notes));
  };

  const addNote = async (title) => {
    const note = {
      title,
      date: new Date().toJSON(),
      completed: false,
    };

    const { data } = await axios.post(`${API_CONFIG.baseUrl}/notes.json`, note);

    dispatch(addNoteAction(note, data.name));
  };

  const removeNote = async (id) => {
    await axios.delete(`${API_CONFIG.baseUrl}/notes/${id}.json`);
    dispatch(removeNoteAction(id));
  };

  const toggleNote = async (id, completed) => {
    const { data } = await axios.patch(
      `${API_CONFIG.baseUrl}/notes/${id}.json`,
      { completed: !completed }
    );
    dispatch(toggleNoteAction(id));
    console.log(state.notes);
  };

  return (
    <FirebaseContext.Provider
      value={{
        showLoader,
        addNote,
        removeNote,
        fetchNotes,
        toggleNote,
        loading: state.loading,
        notes: state.notes,
      }}
    >
      {children}
    </FirebaseContext.Provider>
  );
};
