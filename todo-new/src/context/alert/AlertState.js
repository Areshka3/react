import React, { useReducer } from "react";
import { AlertContext } from "./alertContext";
import { alertReducer } from "./AlertReducer";
import { hideAlertAction, showAlertAction } from "./actions";

export const AlertState = ({ children }) => {
  const [state, dispatch] = useReducer(alertReducer, { visible: false });

  const showAlert = (text, type = "warning") => {
    dispatch(showAlertAction(text, type));
    setTimeout(() => dispatch(hideAlertAction()), 5000);
  };

  const hideAlert = () => dispatch(showAlertAction());

  return (
    <AlertContext.Provider value={{ showAlert, hideAlert, alert: state }}>
      {children}
    </AlertContext.Provider>
  );
};
